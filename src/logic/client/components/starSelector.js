class StarSelector extends HTMLElement {
  constructor() {
    super();

    this.querySelectorAll("input[type='radio']").forEach((checkbox) => {
      checkbox.addEventListener("change", (e) => {
        let current = false;
        this.querySelectorAll("input[type='radio']").forEach((el) => {
          let starSpan = el.parentElement.children[1];
          if (el.checked) {
            current = true;
            this.querySelector(".star-label").innerText = el.dataset.label;
          }
          if (current) starSpan.classList.remove("text-highlight");
          else starSpan.classList.add("text-highlight");
        });
      });
    });
  }
}

export default StarSelector;
