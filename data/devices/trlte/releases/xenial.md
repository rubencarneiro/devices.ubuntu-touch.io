---
maturity: .6
---

<section id="preinstall">

1. You need to ensure you have an unlocked bootloader. The Verizon variant should be the only variant with a [locked bootloader](https://forum.xda-developers.com/t/howto-bootloader-unlock-and-upgrade-to-marshmallow-n910vvru2cql1.3398144/), there is currently no solution for unlocking the AT&T variant.
2. Ensure you have [Installed TWRP](https://twrp.me/samsung/samsunggalaxynote4qualcomm.html).
3. [Enable developer options](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/) in the Android settings.
4. In the developer options, [enable ADB debugging](https://wiki.lineageos.org/adb_fastboot_guide.html) and OEM unlocking.

</section>
