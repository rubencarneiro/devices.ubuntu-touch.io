---
variantOf: "gram"
name: "Xiaomi Redmi Note 9S/9 Pro (India)"
description: "The Redmi Note 9S/9 Pro (India) is a good level Android smartphone, nice for photos, that can satisfy even the most demanding of users."

deviceInfo:
  - id: "rearCamera"
    value: "64MP(wide), 8MP(ultrawide), 5MP(macro), 2MP(depth)"
  - id: "releaseDate"
    value: "March 2020"
---
