<section id="preinstall">

- If the device is running an Android version above 9.0, you have to downgrade to the last 9.0 release. Make sure to revert to [factory image PQ3B.190801.002](https://developers.google.com/android/images#bonito) before continuing.

</section>
