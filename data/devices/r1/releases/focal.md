---
portType: "Halium 12.0"
kernelVersion: "4.19.191"

portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "x" # Not supported
      - id: "torchlight"
        value: "x" # HW not present
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "x" # HW not present
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+-" # Stepmotor can't switch to front cam
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+-"
      - id: "dualSim"
        value: "x" # This is single sim device
      - id: "calls"
        value: "x" # Not supported on stock either
      - id: "mms"
        value: "x" # Not supported on stock either
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "x" # not supported on stock
      - id: "audioRoutings"
        value: "x" # Not supported on stock
      - id: "voiceCall"
        value: "x" # Not supported
      - id: "volumeControl"
        value: "x" # Not supported
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "waydroid"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "x" # HW not present
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "x" # HW not present
      - id: "wirelessExternalMonitor"
        value: "-" # aethercast not working
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "fmRadio"
        value: "x" # Not supported by HW
      - id: "hotspot"
        value: "+"
      - id: "nfc"
        value: "x" # HW not present
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "x" # Not supported by HW
      - id: "fingerprint"
        value: "x" # Not supported by HW
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
      - id: "dt2w"
        value: "x" # Not supported by HW
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+-" # Sometimes works sometimes does not
      - id: "adb"
        value: "+-" # Sometimes works sometimes does not
      - id: "wiredExternalMonitor"
        value: "x"
---
